# utils.py
# several utilities for citotron
# 1. isbn validator

import re


def is_valid_isbn(isbn):
    """
    It takes a isbn string and return
    True if it's a valid ISBN
    """
    # Best way to test for in bash is:
    # grep isbn cites.csv | awk --field-separator '\t' '{print $6}' | grep -P $REGEX
    # Laxer:
    # regex = re.compile("^(\d{9})(\d|X)$|^(\d{13})$")
    # Stricter:
    regex = re.compile("^(\d{9})(\d|X)$|^(978|979)(\d{10})$")

    if not regex.search(isbn):
        return False
        
    # Split into a list
    chars = list(isbn)
    # Remove the final ISBN digit from `chars`, and assign it to `last`
    last = chars.pop()

    if len(chars) == 9:
        # Compute the ISBN-10 check digit
        val = sum((x + 2) * int(y) for x,y in enumerate(reversed(chars)))
        check = 11 - (val % 11)
        if check == 10:
            check = "X"
        elif check == 11:
            check = "0"

    else:
        # Compute the ISBN-13 check digit
        val = sum((x % 2 * 2 + 1) * int(y) for x,y in enumerate(chars))
        check = 10 - (val % 10)
        if check == 10:
            check = "0"

    return (str(check) == last)
