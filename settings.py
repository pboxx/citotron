# Citotron settings

## General

datadir = "data"
logfile = "log.csv"
scientific_publishers = "scientific_publishers.lst"
journal_abbrevs = "journal_abbrevs.json"
partial_journal_abbrevs = "journal_partial_abbrevs.json"
pmid_journal_abbrevs = "pmid_journal_abbrevs.json"

## Resolver urls

doi0 = "http://su8bj7jh4j.search.serialssolutions.com/?id=DOI"

doi1 = "http://search.crossref.org/dois?q=DOI"

isbn0 = "http://www.amazon.com/gp/search/ref=sr_adv_b/?search-alias=stripbooks&unfiltered=1&field-isbn=ISBN&field-dateop=During&sort=relevanceexprank&Adv-Srch-Books-Submit.x=22&Adv-Srch-Books-Submit.y=5"

isbn1 = "https://www.worldcat.org/search?q=bn%3AISBN&qt=advanced&dblist=638"

isbn2 = "http://www.bookfinder.com/search/?isbn=ISBN&st=xl&ac=qr"

isbn3 = "http://www.isbnsearch.org/isbn/ISBN"

isbn4 = "http://openlibrary.org/api/books?bibkeys=ISBN:ISBN&details=true"

# For checking if an ISBN is "scientific" or not
isbn10 = "http://wokinfo.com/cgi-bin/bkci/search.cgi"

pmid0 = "http://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi?db=pubmed&id=PMID"

pmc0 = "http://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi?db=pmc&id=PMC"

arxiv0 = "http://arxiv.org/abs/ARXIV"

arxiv1 = "http://export.arxiv.org/api/query?id_list=ARXIV"
