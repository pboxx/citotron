#!/usr/bin/python3
# Wikipedia citation tools

__version__ = '1.0.0'

from argparse import ArgumentParser
from bs4 import BeautifulSoup as bs
from collections import Counter as counter
from isbnlib import mask
from itertools import repeat
from joblib import Parallel, delayed
from poliplot import *
from pprint import pprint as pprint
from requests import get, post
from time import sleep
from utils import is_valid_isbn
import csv, json, os, sys, re
import settings as s

# ---- Random note:
#
#  Patch requests library to do retries:
# 
#  import requests
#  s = requests.Session()
#  a = requests.adapters.HTTPAdapter(max_retries=3)
#  s.mount('http://', a)
        
# ---- HELPERS ----

def pmid_journal_abbrevs(filename=s.pmid_journal_abbrevs):
    with open(os.path.join(s.datadir, filename),
              mode='rt',
              encoding='utf-8') as f:
        return {canonical(k):canonical(v) for k,v in json.load(f).items()}


def journal_abbrevs(filename=s.journal_abbrevs):
    with open(os.path.join(s.datadir, filename),
              mode='rt',
              encoding='utf-8') as f:
        return {canonical(k):canonical(v) for k,v in json.load(f).items()}


def partial_journal_abbrevs(filename=s.partial_journal_abbrevs):
    with open(os.path.join(s.datadir, filename),
              mode='rt',
              encoding='utf-8') as f:
        return {canonical(k):canonical(v) for k,v in json.load(f).items()}


def print_fails():
    print("=" * 80)
    print("Fails by kind:")
    for k,v in kindsfails.items():
        print("    %s: %s" % (k,v))
    print("Fails by resolver:")
    for k,v in resolversfails.items():
        print("    %s: %s" % (k,v))
    print("Processed rows: %s" % total)
    print("Total fails: %s" % fails)
    print("Total successes: %s" % successes)
    print("Fail rate:", str((fails / total) * 100)[:4] + "%")


def safeget(url):
    try:
        return get(url, timeout=args.timeout)
    except:
        raise


def safepost(url, data):
    try:
        return post(url, timeout=args.timeout, data=data)
    except:
        raise


def progress(character):
    """Convenient dot machine type printer."""
    if args.debug:
        print(character, end='', flush=True)


def safeprint(*args):
    """Convert arguments to strings, False to "ERROR", print the result."""
    print(*[str(arg) if arg is not False else 'ERROR' for arg in args], sep="")


def lastreplace(s, old, new):
    li = s.rsplit(old, 1)
    return new.join(li)


def scientific_publishers():
    """Return a list of scientific publishers read from file."""
    with open(os.path.join(s.datadir, s.scientific_publishers),
              mode='rt', encoding='utf-8') as f:
        publishers = [l.strip() for l in f.readlines()]
    return publishers


def canonical(title):
    """Bring the title to a canonical form"""
    return re.sub("[^a-zA-z0-9]+", " ", title).strip()


def expand(title):
    """Expand abbreviations in title."""
    if title in journal_abbrevs.keys():
        return journal_abbrevs[title].strip()
    if title in pmid_journal_abbrevs.keys():
        return  pmid_journal_abbrevs[title].strip()
    title += ' '
    for k,v in partial_journal_abbrevs.items():
        if k+' ' in title:
            title = title.replace(k,v)
    return title.strip()

# ---- GENERAL FUNCTIONS ----

def resolve_row(row, serial, n=0):
    '''
    Resolves title for row, where row has uid = row['id'] and kind = row['type'].
    - Calls resolver function resolve_<kind><n>.
    - If resolver returns string, it returns it as the title.
    - Else, if resolver returns False, it tries resolve_<kind><n+1>, ...
    - Finally, if resolver does not exist, it returns False.
    '''
    global total, successes, fails
    resolver = 'resolve_' + row['type'] + str(n)
    try:
        resolved = globals()[resolver](row['id'])
        safeprint("-" * 80)
        safeprint("[", str((serial / total) * 100)[:4],"%] ",
                  serial, "/", total,
                  " (", successes, " successes / ", fails, " fails) ",
                  str((fails / serial) * 100)[:4],"% error rate."
                  " Cc: ", resolver, ":", "\n",
                  "    ", resolved)
        sleep(args.sleep)
        if resolved:
            resolved = expand(canonical(resolved))
            log.writerow([resolved])
            print("   ", resolved)
            successes += 1
        return resolved or resolve_row(row, serial, n + 1)
    except KeyError:
        fails += 1
        kindsfails[row['type']] += 1
        return False


def resolve_rows(rows, sci=False):
    """Resolve rows to titles in parallel."""
    n = 8 if sci else 0
    return (Parallel(n_jobs=args.jobs,
                     backend='threading')
            (delayed(resolve_row)(row, serial, n) for row, serial, n in
             zip(rows, range(1, len(rows)), repeat(n))))


# ---- RESOLVERS ----

def resolve_doi0(doi):
    """Doi to title or False."""
    global resolversfails
    url = lastreplace(s.doi0, 'DOI', doi.replace('/', '%2F'))
    try:
        soup = bs(safeget(url).text, 'html5lib')
        citation_data = dict(zip([x.text.strip() for x in soup.select(".section .citation-data .span3")],
                                 [x.text.strip() for x in soup.select(".section .citation-data .span9")]))
        return citation_data['Journal:']
    except:
        resolversfails['doi0'] += 1
        return False

        
def resolve_doi1(doi):
    """Doi to title or False."""
    global resolversfails
    url = lastreplace(s.doi1, 'DOI', doi.replace("/", "%2F"))
    try: 
        j = json.loads(safeget(url).text)
        if j:
            return bs(j[0]['fullCitation'], 'html5lib').select('i')[0].text
        else:
            resolversfails['doi1'] += 1
            return False
    except:
        resolversfails['doi1'] += 1
        return False


# Amazon Advanced Search lookup
def resolve_isbn0(isbn):
    """Isbn to title or False."""
    global resolversfails
    url = lastreplace(s.isbn0, 'ISBN', isbn)
    # print('URL: ' + url)
    try:
        soup = bs(safeget(url).text, 'html5lib')
        title = soup.select('.s-access-detail-page h2')[0].text
        return title
    except:
        resolversfails['isbn0'] += 1
        return False

    
# WorldCat Advanced search lookup
def resolve_isbn1(isbn):
    """Isbn to title or False."""
    global resolversfails
    url = lastreplace(s.isbn1, 'ISBN', isbn)
    # print('URL: ' + url)
    try:
        soup = bs(safeget(url).text, 'html5lib')
        title = soup.select(".name a strong")[0].text
        if not "ISBN" in title:
            return title
        else:
            resolversfails['isbn1'] += 1
            return False
    except:
        resolversfails['isbn1'] += 1
        return False


# Bookfinder API
def resolve_isbn2(isbn):
    """Isbn to title or False."""
    global resolversfails
    url = lastreplace(s.isbn2, 'ISBN', isbn)
    try:
        soup = bs(safeget(url).text, 'html5lib')
        title = soup.title.text.strip()
        if title and (('Search error' not in title) and ('ISBN' not in title) and ('BookFinder.com: Forbidden' not in title)):
            return title.split(' by ')[0]
        else:
            resolversfails['isbn2'] += 1
            return False
    except:
        resolversfails['isbn2'] += 1
        return False


# Isbnsearch API
def resolve_isbn3(isbn):
    """Isbn to title or False."""
    global resolversfails
    url = lastreplace(s.isbn3, 'ISBN', isbn)
    # print('URL: ' + url)
    try:
        soup = bs(safeget(url).text, 'html5lib')
        title = soup.title.text
        return title.split('|')[1].split('(')[0].strip()
    except:
        resolversfails['isbn3'] += 1
        return False


# Open Library API
# http://openlibrary.org/api/books?bibkeys=ISBN:ISBN&details=true
def resolve_isbn4(isbn):
    """Isbn to title or False."""
    global resolversfails
    url = lastreplace(s.isbn4, 'ISBN', isbn)
    # print('URL: ' + url)
    try:
        j = json.loads(safeget(url).text.replace('var _OLBookInfo = ', '')[:-1])
        return j['ISBN:0856651400']['details']['title']
    except:
        resolversfails['isbn4'] += 1
        return False


# Thomson Reuters Web of Knowledge Book Master List
# http://wokinfo.com/cgi-bin/bkci/search.cgi
# Match: 978-0-415-59181-2
def resolve_isbn8(isbn):
    global resolversfails
    try:
        soup = bs(safepost(s.isbn8, data={'search': mask(isbn), 'searchtype': 'and'}).text, 'html5lib')
        return soup.find_all('td', valign="top")[5].text.rstrip()
    except:
        resolversfails['isbn8'] += 1
        return False


def resolve_pmid0(pmid):
    """Pmid to title or False"""
    global resolversfails
    url = lastreplace(s.pmid0, 'PMID', pmid)
    try:
        soup = bs(safeget(url).text, 'html5lib')
        return soup.find('item', {'name':"Source"}).text
    except:
        resolversfails['pmid0'] += 1
        return False

    
def resolve_pmc0(pmc):
    """Pmc to title or False"""
    global resolversfails
    url = lastreplace(s.pmc0, 'PMC', pmc)
    try:
        soup = bs(safeget(url).text, 'html5lib')
        return soup.find('item', {'name':"Source"}).text
    except:
        resolversfails['pmc0'] += 1
        return False

    
def resolve_arxiv0(arxiv):
    """Arxiv to title or False."""
    global resolversfails
    url = lastreplace(s.arxiv0, 'ARXIV', arxiv)
    try:
        soup = bs(safeget(url).text, 'html5lib')
        doi = soup.find('meta', {'name':"citation_doi"})['content']
        return resolve_doi0(doi) or resolve_doi1(doi)
    except:
        resolversfails['arxiv0'] += 1
        return False

    
def resolve_arxiv1(arxiv):
    """Arxiv to title or False."""
    global resolversfails
    url = lastreplace(s.arxiv1, 'ARXIV', arxiv)
    try:
        soup = bs(safeget(url).text, 'html5lib')
        print('Found doi:', soup.find('arxiv:doi').text)
        return resolve_doi0(soup.find('arxiv:doi').text)
    except:
        resolversfails['arxiv1'] += 1
        return False

    
# ---- MISC. FUNCTIONS ----

def isbns_in_libthing(rows):
    '''
    Tells how many ISBNS of rows are in Library Thing.
    '''
    # List of all ISBNs in Library Thing:
    # http://www.librarything.com/feeds/AllLibraryThingISBNs.csv
    libthing = open(os.path.join(s.datadir,
                                 'AllLibraryThingISBNs.csv'), 'r').readlines()
    libthing = [x.rstrip() for x in libthing]
    isbns = [row['id'] for row in rows if row['type'] == 'isbn']
    hits = 0
    for isbn in isbns:
        if isbn in libthing:
            hits += 1
    print("We have %s ISBNS and only %s of them are in Library Thing." % (str(len(isbns)), str(hits)))
    return hits


def count_titles(titles):
    '''
    Accepts a list of titles, logging and returning their frequencies.
    '''
    freq = counter(titles).most_common()
    for k, v in freq:
        out.writerow([k, v])
    return freq


def count_types(rows):
    """Counts how many rows of each kind (isbn, doi, etc.) are in the input csv."""
    types = []
    for row in rows:
        types.append(row['type'])
    return counter(types).most_common()


def filter_kind(kind, rows):
    return [row for row in rows if row['type'] == kind]


def scisbn(rows):
    """Returns statistics about which ISBNs are scientific"""
    global successes, fails
    rows = newrows
    resolve_rows(rows, sci=True)
    return {'All ISBNs': len(rows),
            'Scientific ISBNs': successes,
            'Non-scientific ISBNs': fails}

if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument("-m", "--mode", dest="mode", default="count",
                        help="Action to perform on data. 'count' counts the frequency of publication (book, journal, etc.) titles in the dataset. 'resolve' only resolves identifiers to titles (used for testing). 'types' counts the frequency of the identifiers themselves in the dataset (used for understanding the dataset).")
    parser.add_argument("-k", "--kind", dest="kind", default="all",
                        help="Kind of identifier to process. Default is 'all'. Current choices are 'isbn', 'pmid', 'pmd', 'doi', 'arxiv'.")

    parser.add_argument("-i", "--inputfile", dest="inputfile", default="input.csv",
                        help="Input filename. Default is 'input.csv'.")

    parser.add_argument("-c", "--comparefile", dest="comparefile", default="other.csv",
                        help="Compare with filename. Default is 'other.csv'.")

    parser.add_argument("-o", "--outputfile", dest="outputfile", default="output.csv",
                        help="Output filename. Default is 'output.csv'.")

    parser.add_argument("-d", "--discriminant", dest="discriminant", default=10, type=int,
                        help="Plotting parameter. Exclude results when frequency is lower than this constant. Use in combination with 'count' mode. Default is 10.")
    
    parser.add_argument("-s", "--sleep", dest="sleep", default=2, type=int,
                        help="Sleep between requests. Default is 2.")

    parser.add_argument("-t", "--timeout", dest="timeout", default=6, type=int,
                        help="Time to wait for response from server. Default is 6.")

    parser.add_argument("-j", "--jobs", dest="jobs", default=1, type=int,
                        help="Number of parallel requests. Default is 1.")

    parser.add_argument("-v", "--verbose", dest="debug", action="store_true",
                        help="Verbose mode. Default is True.")


    args = parser.parse_args()
    # Set shell output buffering to 1 by reopeining stdout
    sys.stdout = os.fdopen(sys.stdout.fileno(), 'w', 1)

# ---- INIT ----

# Enumerate generator so that we only read the file once
rows = list(csv.DictReader(open(os.path.join(s.datadir, args.inputfile), 'rt'),
                           delimiter="\t"))
log = csv.writer(open(s.logfile, 'w', encoding='utf-8'),
                 delimiter='|',
                 quotechar='"',
                 quoting=csv.QUOTE_MINIMAL)
out = csv.writer(open(args.outputfile, 'w', encoding='utf-8'),
                 delimiter='|',
                 quotechar='"',
                 quoting=csv.QUOTE_MINIMAL)
# Overwrite function names with variable names
journal_abbrevs = journal_abbrevs()
partial_journal_abbrevs = {'False': 'False'} # partial_journal_abbrevs()
pmid_journal_abbrevs = pmid_journal_abbrevs()

# --- NORMALISE ----

# Some ISBNs end with small x, which is against the ISBN specification:
newrows = []
count = 0
for row in rows:
    if row['type'] == 'isbn' and 'x' in row['id']:
        row['id'] = row['id'].replace('x', 'X')
        count += 1
    newrows.append(row)
rows = newrows
print("**** --- Corrected lowercase 'x' character in " + str(count) + " ISBNs --- ****")
    
newrows = [row for row in rows if row['type'] != 'isbn' or (row['type'] == 'isbn' and is_valid_isbn(row['id']))]
count = len(rows) - len(newrows)
print("**** ---- Dropped " + str(count) + " invalid ISBNs ---- ****")
rows = newrows

# ---- GLOBAL STATE ----

total, fails, successes = len(rows), 0, 0
kinds = [k.replace('resolve_', '')[:-1] for k in globals().keys() if 'resolve' in k and '0' in k]
kindsfails = {k:v for k, v in zip(kinds, repeat(0))}
resolvers = [k.replace('resolve_','') for k in globals().keys() if 'resolve' in k and 'row' not in k]
resolversfails = {k:v for k, v in zip(resolvers, repeat(0))}

# ---- CALLS ----

if args.kind != 'all':
    rows = filter_kind(args.kind, rows)
    total = len(rows)

m = args.mode
if m == "count":
    pprint(count_titles(resolve_rows(rows)))
    print_fails()
    plot_occurrences(args.outputfile, args.discriminant)
elif m == "resolve":
    resolve_rows(rows)
    print_fails()
elif m == "types":
    pprint(count_types(rows))
elif m == "scisbn":
    pprint(scisbn(filter_kind('isbn', rows)))
else:
    print("mode was: '" + str(m) + "'")
    print("="*80)
    print("You have to choose a supported mode!")
    print("Try to run the script without arguments to get a help message.")
